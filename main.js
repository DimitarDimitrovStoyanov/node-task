const Database = require('./config/database'),
      mongodb = require('mongodb'),
      soap = require('./config/soap'),
      port = 3000;

let db = new Database(mongodb);

db.connect()
  .then((db) => require('./data/data.js').init(db, soap))
  .then((data) => require('./config/app')(data))
  .then((app) => {
    app.listen(port, () => {
        console.log("The server is running on port : " + port);
    });
  });