let mocha = require('mocha'),
    chai = require('chai'),
    sinon = require('sinon'),
    CurrencyController = require('../../../controllers/currency-controller.js');

const expect = chai.expect;

describe('Currency controller tests', function() {
    let currencyData,
        expectedErrorMessage,
        data,
        req,
        res,
        controller;

        beforeEach(function() {
            req = {
                body: {
                    currentCurrency: 'currentCurrency',
                    wantedCurrency: 'wantedCurrency',
                    value: 'value'
                }
            };
            res = {
                json: () => {}
            }
            expectedErrorMessage = 'The passed currency-data service is not valid.';
            currencyData = {
                convert: () => {
                    return Promise.resolve();
                }
            };
            data = {
                currency: currencyData
            };
            controller = new CurrencyController(data);
        });

        it('CurrencyController should exist', function() {
            expect(CurrencyController).to.exist;
        });

        it('Expect expect to set correctly the property currencyData', function() {
            chai.assert.deepEqual(controller.currencyData, currencyData);
        });
    
        it('Expect to have all functions', function() {
            expect(controller.convert).to.be.a('function');
        });

        describe('Currency data property tests', function() {
            it('Expect to throw error when is passed a null value.', function() {
                expect(() => {
                    controller.currencyData = null;
                }).to.throw(expectedErrorMessage);
            });

            it('Expect to throw error when is passed a undefined value.', function() {
                expect(() => {
                    controller.currencyData = undefined;
                }).to.throw(expectedErrorMessage);
            });
        });

        describe('convert function tests', function() {
            it('Should call this.currencyData.convert()', function() {
                let convertStub = sinon.stub(currencyData, 'convert').returns(Promise.resolve({}));

                controller.convert(req, res);

                sinon.assert.calledOnce(convertStub);
            });

            it('Should call res.json()', function() {
                let jsonStub = sinon.stub(res, 'json'),
                    convertStub = sinon.stub(currencyData, 'convert').returns(Promise.resolve({}));

                controller.convert(req, res)
                          .then(() => {
                            sinon.assert.calledOnce(jsonStub);
                          });
            });
        });


});