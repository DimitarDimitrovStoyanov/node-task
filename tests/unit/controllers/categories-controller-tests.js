let mocha = require('mocha'),
    chai = require('chai'),
    sinon = require('sinon'),
    CategoriesController = require('../../../controllers/categories-controller.js');

const expect = chai.expect;

describe('Products controller tests', function() {
let categoriesData,
    expectedErrorMessage,
    data,
    req,
    res,
    controller,
    categories = [ { primary_category_id: "asdas" } ];

    beforeEach(function() {
        categoriesData = {
            getCategory: (subcategory) => { return Promise.resolve([ { primary_category_id: "asdas" } ])}
        };
        expectedErrorMessage = 'The passed categories-data service is not valid.';
        data = {
            categories:  categoriesData
        };
        req = {
            url: "url"
        };
        res = {
            render: () => {}
        };
        controller = new CategoriesController(data);
    });

    it('ProductsController should exist', function() {
        expect(CategoriesController).to.exist;
    });

    it('Expect expect to set correctly the property categoriesData', function() {
        chai.assert.deepEqual(controller.categoriesData, categoriesData);
    });

    it('Expect to have all functions', function() {
        expect(controller.loadCategoryPage).to.be.a('function');
    });

    describe('Categories data property tests', function() {
        it('Expect to throw error when is passed a null value.', function() {
            expect(() => {
                controller.categoriesData = null;
            }).to.throw(expectedErrorMessage);
        });

        it('Expect to throw error when is passed a undefined value.', function() {
            expect(() => {
                controller.categoriesData = undefined;
            }).to.throw(expectedErrorMessage);
        });
    });

    describe('loadCategoryPage function tests', function() {
        it('Should call this.categoriesData.getCategory()', function() {
            let getCategoryStub = sinon.stub(categoriesData, 'getCategory').returns(Promise.resolve(categories));

            controller.loadCategoryPage(req, res);

            sinon.assert.calledOnce(getCategoryStub);
        });

        it('Should call res.render()', function() {
            let renderStub = sinon.stub(res, 'render');

            controller.loadCategoryPage(req, res)
                      .then(() => {
                        sinon.assert.calledOnce(renderStub);
                      });
        });
    });


});