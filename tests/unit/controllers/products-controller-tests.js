let mocha = require('mocha'),
    chai = require('chai'),
    sinon = require('sinon'),
    ProductsController = require('../../../controllers/products-controller.js');

const expect = chai.expect;

describe('Products controller tests', function() {
    let productsData,
        expectedErrorMessage,
        data,
        req,
        res,
        controller,
        products = [ { primary_category_id: "asdas" } ],
        product = { primary_category_id: "asdas" };
    
    beforeEach(function() {
        productsData = {
            getProductsFromACategory: (subcategory) => { return Promise.resolve(products) },
            getProductByName: () => {}
        };
        expectedErrorMessage = 'The passed products-data service is not valid.';
        data = {
            products:  productsData
        };
        req = {
            params: {
                subcategory: 'subcategory',
                name: 'name'
            }
        };
        res = {
            render: () => {}
        };
        controller = new ProductsController(data);
    });
    


    it('ProductsController should exist', function() {
        expect(ProductsController).to.exist;
    });

    it('Expect expect to set correctly the property productsData', function() {
        chai.assert.deepEqual(controller.productsData, productsData);
    });

    it('Expect to have all functions', function() {
        expect(controller.loadProductsPage).to.be.a('function');
        expect(controller.loadGetGivenProductPage).to.be.a('function');
    });

    describe('Products data property tests', function() {
        it('Expect to throw error when is passed a null value.', function() {
            expect(() => {
                controller.productsData = null;
            }).to.throw(expectedErrorMessage);
        });

        it('Expect to throw error when is passed a undefined value.', function() {
            expect(() => {
                controller.productsData = undefined;
            }).to.throw(expectedErrorMessage);
        });
    });

    describe('loadProductsPage function tests', function() {
        it('Should call this.productsData.getProductsFromACategory()', function() {
            let getProductsFromACategoryStub = sinon.stub(productsData, 'getProductsFromACategory').returns(Promise.resolve(products));

            controller.loadProductsPage(req, res);

            sinon.assert.calledOnce(getProductsFromACategoryStub);
        });

        it('Should call res.render()', function() {
            let renderStub = sinon.stub(res, 'render');

            controller.loadProductsPage(req, res)
                      .then(() => {
                        sinon.assert.calledOnce(renderStub);
                      });
            
        });
    });

    describe('loadGetGivenProductPage function tests', function() {
        it('Should call this.productsData.getProductByName()', function() {
            let getGivenProductStub = sinon.stub(productsData, 'getProductByName').returns(Promise.resolve(product));

            controller.loadGetGivenProductPage(req, res);

            sinon.assert.calledOnce(getGivenProductStub);
        });

        it('Should call res.render()', function() {
            let renderStub = sinon.stub(res, 'render'),
                getGivenProductStub = sinon.stub(productsData, 'getProductByName').returns(Promise.resolve(product));;

            controller.loadGetGivenProductPage(req, res)
                      .then(() => {
                        sinon.assert.calledOnce(renderStub);
                      });
        });
    });
});
