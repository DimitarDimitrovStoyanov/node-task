const CategoriesController = require('../controllers/categories-controller.js');

module.exports = function(router, data) {
    let controller = new CategoriesController(data);

    router.get('/', (req, res) => res.redirect('/mens'))
          .get('/mens', (req, res) => controller.loadCategoryPage(req, res))
          .get('/womens', (req, res) => controller.loadCategoryPage(req, res));
}