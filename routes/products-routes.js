const ProductsController = require('../controllers/products-controller.js');
module.exports = function(router, data) {
    let productController = new ProductsController(data);

    router.get('/products/:subcategory', (req, res) => productController.loadProductsPage(req, res))
          .get('/product/:name', (req, res) => productController.loadGetGivenProductPage(req, res));
}