const CurrencyController = require('../controllers/currency-controller');

module.exports = function(router, data) {
    let controller = new CurrencyController(data);

    router.post('/convertCurrency', (req, res) => controller.convert(req, res));
}