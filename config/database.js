class Database {
    constructor(mongodb) {
        this.mongodb = mongodb;
    }

    get mongodb() {
        return this._mongodb;
    }

    set mongodb(value) {
        if(value === undefined || value === null) {
            throw 'Invalid value is passed to the mongoose property.'
        }

        this._mongodb = value;
    }

    connect() {
       return this.mongodb
                  .MongoClient
                  .connect('mongodb://localhost:27017/shop')
                  .then((db) => {
                      console.log('Connected to the database.');

                      return db;
                  });
    }
}

module.exports = Database;