module.exports = function(data) {
    const express = require('express'),
          app = express(),
          path = require('path'),
          cookieParser = require('cookie-parser'),
          expressSession = require('express-session'),
          bodyParser = require('body-parser'),
          libsPath = path.join(__dirname, '../node_modules'),
          staticsPath = path.join(__dirname, '../public'),
          { Router } = require('express');

    app.set('view engine', 'pug');
    app.set('views', './views/');

    app.use('/libs', express.static(libsPath));
    app.use('/static', express.static(staticsPath));

    app.use(cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(expressSession({
        secret: 'shopt',
        cookie: { maxAge: 60 * 60 * 60 * 1000 },
        rolling: true,
        resave: true,
        saveUninitialized: false,
    }));

    // router configuration
    let router = new Router();

    require('../routes/categories-routes')(router, data);
    require('../routes/products-routes')(router, data);
    require('../routes/currency-routes')(router, data);

    app.use(router);

    // security configuration
    require('./security')(app);

    app.use(function(req, res, next) {
        res.status(404)
           .render('page-not-found.pug');
    });

    return app;
};