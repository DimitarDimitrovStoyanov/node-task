class CurrencyController {
    constructor(data) {
        this.currencyData = data.currency;
    }

    get currencyData() {
        return this._currencyData;
    }

    set currencyData(value) {
        if(typeof value !== 'object' || value === null) {
            throw 'The passed currency-data service is not valid.'
        }

        this._currencyData = value;
    }

    convert(req, res) {
        let currentCurrencyMoneda = req.body.currentCurrency, 
            wantedCurrencyMoneda = req.body.wantedCurrency,
            value = req.body.value;
        
        return this.currencyData.convert(currentCurrencyMoneda, wantedCurrencyMoneda, value)
                                .then((convertedCurrency) => {
                                   res.json({
                                       convertedCurrency
                                   });
                                });
    }
}

module.exports = CurrencyController;