class CategoriesController {
    constructor(data) {
        this.categoriesData = data.categories;
    }

    get categoriesData() {
        return this._categoriesData;
    }

    set categoriesData(value) {
        if(typeof value !== 'object' || value === null) {
            throw 'The passed categories-data service is not valid.'
        }

        this._categoriesData = value;
    }

    loadCategoryPage(req, res) {
        let categoryName = req.url.split('/')[1];

        return this.categoriesData.getCategory(categoryName)
                                  .then((mainCategory) => {
                                    res.render('main-category-page.pug', {
                                       mainCategory: mainCategory
                                    });
                                  });
    }
}

module.exports = CategoriesController;