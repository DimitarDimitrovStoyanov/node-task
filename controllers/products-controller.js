class ProductsController {
    constructor(data) {
        this.productsData = data.products;
    }

    get productsData() {
        return this._productsData;
    }

    set productsData(value) {
        if(typeof value !== 'object' || value === null) {
            throw 'The passed products-data service is not valid.'
        }

        this._productsData = value;
    }

    loadProductsPage(req, res) {
        let subcategory = req.params.subcategory;

        return this.productsData.getProductsFromACategory(subcategory)
                                .then((products) => {
                                   res.render('products-page.pug', {
                                       products: products,
                                       category: products[0].primary_category_id.split('-')[0],
                                       subcategory: subcategory
                                   });
                                });
    }

    loadGetGivenProductPage(req, res) {
        let productName = req.params.name;

        return this.productsData.getProductByName(productName)
                                .then((product) => {
                                   let productCategoryArray = product.primary_category_id.split('-'),
                                       category = productCategoryArray[0],
                                       subcategory = productCategoryArray[0] + "-" +  productCategoryArray[1]
                                
                                   res.render('given-product-page.pug', {
                                       product: product,
                                       category: category,
                                       subcategory: subcategory
                                   });
                                });
    }
}

module.exports = ProductsController;