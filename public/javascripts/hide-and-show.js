$(document).ready(function(){
    $(".main-category").on("click", function(ev){
        let children = $(ev.target).children(".categories");

        if($(children).hasClass("d-none")) {
            $(children).removeClass("d-none");
            $(children).addClass("d-block");
        } else {
            $(children).addClass("d-none");
            $(children).removeClass("d-block");
        }
    });
});