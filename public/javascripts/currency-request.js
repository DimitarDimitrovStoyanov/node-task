$(document).ready(function(){
    let currentCurrency = $( ".custom-select option:selected").text();

    $('select').change(function(ev) {
        let wantedCurrency = $( ".custom-select option:selected").text(),
            value = $(".price").text().split(':')[1],
            url = '/convertCurrency';

        $.ajax({
            url: url,
            method: "POST",
            data: {
                currentCurrency,
                wantedCurrency,
                value
            },
            dataType: "json"
        }).done((result) => {
            $('.price').text('Price: ' + result.convertedCurrency.toFixed(2));

            currentCurrency = wantedCurrency;
        });
    })
});