# Node task #

## Start the project ##
    - Start mongod.exe file.
	- Execute npm install command.
	- Execute npm start command and the server will start on a port 3000.
## Start the tests ##
    - Execute npm test command.
	
## Database ##
	- Import the products.json and categories.json files (which are in the Db folder) in the database. You can see how to do that using the link below: 
	                https://docs.mongodb.com/getting-started/shell/import-data/