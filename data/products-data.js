class ProductsData {
    constructor(db) {
        this.db = db;
    }

    get db() {
        return this._db;
    }

    set db(value) {
        if(value === undefined || value === null) {
            throw 'The passed database object is not valid';
        }

        this._db = value;
    }

    getProductsFromACategory(category) {
        let query;

        if (category.includes("womens")) {
            query = { primary_category_id : { $regex : ".*" + category  + ".*"}};
        } else {
            query = { primary_category_id : { $not: /womens/, $regex : ".*" + category  + ".*" }};
        }

        return this.db.collection('products')
                      .find(query)
                      .toArray()
                      .then((products) => {
                          return  products;
                      });
    }

    getProductByName(name) {
        return this.db.collection('products')
                      .findOne({ name: name })
                      .then((product) => {
                        return product;
                      });
    }
}

module.exports = ProductsData;