class CategoriesData {
    constructor(db) {
        this.db = db;
    }

    get db() {
        return this._db;
    }

    set db(value) {
        if(value === undefined || value === null) {
            throw 'The passed database object is not valid';
        }

        this._db = value;
    }

    getCategory(categoryName) {
        return this.db.collection('categories')
                      .findOne({ id: categoryName })
                      .then((category) => {
                          return category;
                      });
    }
}

module.exports = CategoriesData;