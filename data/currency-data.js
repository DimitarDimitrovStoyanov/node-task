class CurrencyData {
    constructor(soap) {
        this.soap = soap;
    }

    getAll() {
        return this.soap
                   .then((client) => {
                     return client.getallAsync({ dt: new Date().toJSON() });
                   });
    }

    getValue(dateTime, currencyMoneda) {
        let options  = {
            TheDate: dateTime,
            Moneda: currencyMoneda
        };

        return this.soap  
                   .then((client) => {
                     return client.getvalueAsync(options);
                   });
    }

    getLatestValue(currencyMoneda) {
        let options  = {
            Moneda: currencyMoneda
        };

        return this.soap  
                   .then((client) => {
                     return client.getlatestvalueAsync(options);
                   });
    }


    convert(currentCurrencyMoneda, wantedCurrencyMoneda, value) {
        let currencies,
            currentCurrency,
            wantedCurrency,
            rumunLeis,
            wantedCurrencyValue;

        return this.getAll()
                   .then((result) => {
                        currencies = result.getallResult.diffgram.DocumentElement.Currency,

                        currencies.forEach((currency) => {
                            if(currency.IDMoneda === currentCurrencyMoneda) {
                                currentCurrency = currency;
                            } else if(currency.IDMoneda === wantedCurrencyMoneda) {
                                wantedCurrency = currency;
                            }
                        });

                        rumunLeis = currentCurrency.Value * value;
                        wantedCurrencyValue = rumunLeis / wantedCurrency.Value; 
                       
                        return wantedCurrencyValue;
                   });
            
    }
}

module.exports = CurrencyData;