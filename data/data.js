const ProductsData = require('./products-data'),
      CategoriesData = require('./categories-data'),
      CurrencyData = require('./currency-data');

const init = (db, soap) => {
    return Promise.resolve({
        products: new ProductsData(db),
        categories: new CategoriesData(db),
        currency: new CurrencyData(soap)
    });
};

module.exports = { init };